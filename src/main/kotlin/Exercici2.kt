import kotlinx.coroutines.*
import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/04/15
*/

fun main() {
    val scanner= Scanner(System.`in`)

    runBlocking {
        println("Escriu el nª de cops que vols escriure Hello World")
        val num = scanner.nextInt()
        repetition(num)
    }
    println("Finished")
}
suspend fun repetition(num :Int) {
    coroutineScope {
        for (i in 1..num) {
            launch {
                println("$i -> HelloWord")
            }
            delay(100)
        }
    }
}
