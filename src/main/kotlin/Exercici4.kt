import kotlinx.coroutines.*
import java.util.*
import kotlin.system.exitProcess


/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/04/15
*/

val scanner= Scanner(System.`in`)

fun main() {
    println("Encerta e numero entre 1 i el 50 en meys de 10 segons")
    runBlocking {
     GlobalScope.launch{
              guessNumber()
          }
        delay(10000)
        println("\nTemps finalitzat")
    }
}

fun guessNumber(){
    val randomNumber = (1..50).random()

    do{
        print("Numero: ")
        val userNum = scanner.nextInt()

        if (userNum != randomNumber){
            println("Incorrecte\n")
        }

    }while (userNum != randomNumber)
    println("Ho has aconseguit!")
    exitProcess(0)
}