import kotlinx.coroutines.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/04/15
*/

fun main() {
    runBlocking {
        coroutineScope {
            launch { corroutine1() }
            launch { corroutine2() }
        }
    }
    println("Completed")
}
suspend fun corroutine1() {

    println("Hello World 1.1")
    delay(3000)
    println("Hello World 1.2")
    delay(3000)
    println("Hello World 1.3")
}

suspend fun corroutine2() {

    println("Hello World 2.1")
    delay(2000)
    println("Hello World 2.2")
    delay(2000)
    println("Hello World 2.3")
}
