import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/04/15
*/

fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(300)
        println("The main program is finished")
    }
}

/* SORTIDA
The main program is started
The main program continues
Background processing started
The main program is finished
 */