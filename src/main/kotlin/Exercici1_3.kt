import kotlinx.coroutines.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/04/15
*/

suspend fun main() {
    println("The main program is started")
    doBackgrounds()
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

suspend fun doBackgrounds() {
    withContext(Dispatchers.IO) {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
}