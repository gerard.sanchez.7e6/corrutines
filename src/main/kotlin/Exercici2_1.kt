import kotlinx.coroutines.*
import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/04/15
*/

fun main() {
    val scanner= Scanner(System.`in`)

    runBlocking {
        println("Escriu el nª de cops que vols escriure Hello World")
        val num = scanner.nextInt()

        launch {
            repetitions(num)
        }
    }
    println("Finished")
}
suspend fun repetitions(num :Int) {
    for (i in 1..num) {
        println("$i -> HelloWord")
        delay(1000)
    }

}

