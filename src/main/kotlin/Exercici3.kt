import kotlinx.coroutines.*
import java.io.File
import kotlin.system.measureTimeMillis

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/04/15
*/

fun main() {
    val file = File("src/main/kotlin/data").readLines()
    var time : Long
    runBlocking {
        time = measureTimeMillis {
            for (line in file) {
                withContext(Dispatchers.IO) {
                    if (line.isNotEmpty()) {
                        launch {
                            println(line)
                            delay(3000)
                        }
                    }
                    else{
                        println()
                    }
                }
            }
        }
        println("${time/1000} segons")
    }

}







